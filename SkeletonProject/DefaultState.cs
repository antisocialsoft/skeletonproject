﻿using AssGameFramework.Core;
using AssGameFramework.GameSystems;
using AssGameFramework.SceneGraph;
using AssGameFramework.Source.Model;
using AssGameFramework.States;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkeletonProject
{
    class DefaultState : AbstractState
    {
        private Entity _spriteEntity = null;

        public DefaultState(IApplicationContextCommon context) : base(context)
        {
        }

        protected override void Create()
        {
            // Create logic goes here. Create Entities, models, components. Load content, etc.
            /* START OF SAMPLE LOGIC */
            //Create camera
            Entity cameraEnt = new Entity();
            Camera2DModelPart cameraModel = new Camera2DModelPart();
            cameraEnt.Model.AddPart(cameraModel);
            //Add the camera to the scene
            StateScene.RootEntity.AddChildEntity(cameraEnt);

            //Create sprite
            Entity spriteEnt = new Entity();
            Texture2D spriteTex = Context.Content.Load<Texture2D>("SampleSprites/logosquare");
            SpriteModelPart spriteModel = new SpriteModelPart(spriteTex);
            spriteEnt.Model.AddPart(spriteModel);
            //Add sprite to the scene
            StateScene.RootEntity.AddChildEntity(spriteEnt);

            //Set the member variable for use in update
            _spriteEntity = spriteEnt;
            /* END OF SAMPLE LOGIC */
        }

        /* This function is not required and can be deleted. Used here for some sample logic. */
        public override void StandardUpdate(float deltaTime)
        {
            base.StandardUpdate(deltaTime);
            /* START OF SAMPLE LOGIC */
            _spriteEntity.Transform.LocalRotation += 0.25f * deltaTime;
            /* END OF SAMPLE LOGIC */
        }

        protected override void Destroy()
        {
            // Unload content.
        }

        protected override void OnActive()
        {
            // Unpause, resume audio.
        }

        protected override void OnInactive()
        {
            // Pause, stop audio.
        }

        protected override void PopulateSceneWithSystems(Scene newScene, ComponentSystem compSystem)
        {
            // Create and add systems using newScene.AddSystem()
        }
    }
}
