﻿using AssGameFramework.Core;
using AssGameFramework.States;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace SkeletonProject
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : AssGame
    {
        protected override AbstractState CreateInitialState()
        {
            // Replace the DefaultState with your own
            return new DefaultState(Context);
        }
    }
}
